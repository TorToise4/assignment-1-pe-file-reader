/// @file
/// @brief File with functions for working with PE
#ifndef ASSIGNMENT_1_PE_FILE_READER_MASTER_PEFILE_H
#define ASSIGNMENT_1_PE_FILE_READER_MASTER_PEFILE_H

#endif //ASSIGNMENT_1_PE_FILE_READER_MASTER_PEFILE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/// Reference to offset to skip DOS block
#define MAIN_OFFSET 0x3c

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

/// Structure containing PE header data
struct
#ifdef __GNUC__
__attribute__((packed))
#endif
        PE_header {
    /// Type of target computer
    uint16_t Machine;
    /// Specifies the size of the partition table that immediately follows the headers.
    uint16_t NumberOfSections;
    /// Low 32 bits in seconds since 00:00 January 1, 1970 (run-time value C time_t) indicating when the file was created.
    uint32_t TimeDateStamp;
    /// The offset of the COFF symbol table file, or zero if there is no COFF symbol table.
    uint32_t PointerToSymbolTable;
    /// The number of entries in the symbol table. This data can be used to lay out a string table that immediately follows the symbol table.
    uint32_t NumberOfSymbols;
    /// The size of an optional header that is required for executable files but not for object files.
    uint16_t SizeOfOptionalHeader;
    /// Flags specifying file attributes.
    uint16_t Characteristics;
};

/// Structure containing Section  header data
struct
#ifdef __GNUC__
__attribute__((packed))
#endif
        Section_header{
    /// UTF-8 encoded string with null padding.
    char name[8];
    /// The total size of the partition when loaded into memory. If this value is greater than SizeOfRawData, the section is filled with zero padding.
    uint32_t VirtualSize;
    /// For executable images, the address of the first byte of the section relative to the base of the image when the section is loaded into memory. For feature files, this field is the address of the first byte before the move is applied
    uint32_t VirtualAddress;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files).
    uint32_t SizeOfRawData;
    /// The file pointer to the first page of the section within the COFF file.
    uint32_t PointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations.
    uint32_t PointerToRelocations;
    /// The file pointer to the beginning of line-number entries for the section.
    uint32_t PointerToLinenumbers;
    /// The number of relocation entries for the section. This is set to zero for executable images.
    uint16_t NumberOfRelocations;
    /// The number of line-number entries for the section.
    uint16_t NumberOfLinenumbers;
    /// The flags that describe the characteristics of the section.
    uint32_t Characteristics;
};

/// Structure containing PE file data
struct
#ifdef __GNUC__
__attribute__((packed))
#endif
        PE_file {
    /// Magic number of the file (Signature)
    uint32_t Magic;
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Main header
    struct PE_header header;
    /// Array of section headers with the size of header.number_of_sections
    struct Section_header* sectionHeader;
};



#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Function to read PE
/// @param[in] in input file
/// @param[in] peFile structure containing PE file data
/// @return true in case of successful reading PE
bool read_PE (FILE* in, struct PE_file* peFile);

/// @brief Function to find section by name
/// @param[in] peFile structure containing PE file data
/// @param[in] name name of desired section
/// @return structure containing section header data
struct Section_header* find_section(struct PE_file* peFile, char* name);

/// @brief Function to write desured section from PE file to another file
/// @param[in] out output file
/// @param[in] sectionHeader structure containing section header data
/// @param[in] in input file
/// @return true in case of successful writing
bool write_PE(FILE* out, struct Section_header* sectionHeader, FILE* in);

/// @brief Function to start all action with PE
/// @param[in] in name of income file
/// @param[in] out name of outcome file
/// @param[in] name name of desired section
/// @return true in case of successful executing
bool go(const char * in, const char * out, char* name);
