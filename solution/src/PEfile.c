/// @file
/// @brief File with functions for working with PE

#include "PEfile.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// @brief Function to read PE
/// @param[in] in input file
/// @param[in] peFile structure containing PE file data
/// @return true in case of successful reading PE
bool read_PE (FILE* in, struct PE_file* peFile){
    fseek(in, MAIN_OFFSET, SEEK_SET);
    fread(&peFile->header_offset, sizeof(peFile->header_offset), 1, in);
    fseek(in, (long)peFile->header_offset, SEEK_SET);
    fread(&peFile->Magic, sizeof(peFile->Magic), 1, in);
    fread(&peFile->header, sizeof(struct PE_header), 1, in);
    fseek(in, peFile->header.SizeOfOptionalHeader, SEEK_CUR);
    peFile->sectionHeader = malloc(sizeof(struct Section_header)*peFile->header.NumberOfSections);
    for (size_t i = 0; i < peFile->header.NumberOfSections; i++) {
        fread(&peFile->sectionHeader[i], sizeof (struct Section_header), 1, in);
    }
    return true;
}

/// @brief Function to find section by name
/// @param[in] peFile structure containing PE file data
/// @param[in] name name of desired section
/// @return structure containing section header data
struct Section_header* find_section(struct PE_file* peFile, char* name){
    for (size_t i = 0; i < peFile->header.NumberOfSections; i++) {
        if (strcmp(peFile->sectionHeader[i].name, name) == 0)
            return &peFile->sectionHeader[i];
    }
    return NULL;
}

/// @brief Function to write desured section from PE file to another file
/// @param[in] out output file
/// @param[in] sectionHeader structure containing section header data
/// @param[in] in input file
/// @return true in case of successful writing
bool write_PE(FILE* out, struct Section_header* sectionHeader, FILE* in){
    fseek(in, (long)sectionHeader->PointerToRawData, SEEK_SET);
    void* raw_data = malloc(sectionHeader->SizeOfRawData);
    fread(raw_data, sectionHeader->SizeOfRawData, 1, in);
    fwrite(raw_data, sectionHeader->SizeOfRawData, 1, out);
    free(raw_data);
    return true;
}

/// @brief Function to start all action with PE
/// @param[in] in name of income file
/// @param[in] out name of outcome file
/// @param[in] name name of desired section
/// @return true in case of successful executing
bool go(const char * in, const char * out, char* name){
    if (in == NULL || out == NULL || name == NULL){
        return false;
    }
    FILE* income = fopen( in, "rb");
    FILE* outcome = fopen( out, "wb");
    struct PE_file peFile;
    read_PE(income, &peFile);
    struct Section_header* sectionHeader = find_section(&peFile, name);
    write_PE(outcome,sectionHeader,income);
    free(peFile.sectionHeader);
    return true;
}
