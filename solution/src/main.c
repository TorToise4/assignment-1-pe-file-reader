/// @file 
/// @brief Main application file
#include "PEfile.h"
#include <stdio.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code




int main(int argc, char** argv)
{
    if (argc != 4){
        return 1;
    }
    go(argv[1], argv[3], argv[2]);
    return 0;
}
