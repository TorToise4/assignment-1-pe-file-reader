var struct_p_e__header =
[
    [ "Characteristics", "struct_p_e__header.html#a512bedcdeee57ac112e92a88973b3417", null ],
    [ "Machine", "struct_p_e__header.html#ae6987a88a0d02a2820dc4f2a58b63609", null ],
    [ "NumberOfSections", "struct_p_e__header.html#a6e1b0b40316daaf8671e6dfc7322e5bb", null ],
    [ "NumberOfSymbols", "struct_p_e__header.html#aafd0e5872a9e86c46a1f12362d4fa31a", null ],
    [ "PointerToSymbolTable", "struct_p_e__header.html#aa708d24a8ca6d53aaf5681dacd25675c", null ],
    [ "SizeOfOptionalHeader", "struct_p_e__header.html#afd9d9070d769c72a6b1ce388aeef6080", null ],
    [ "TimeDateStamp", "struct_p_e__header.html#aa50fca3f3231ee97e5efb57819b7d523", null ]
];