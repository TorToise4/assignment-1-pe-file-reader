var _p_efile_8h =
[
    [ "PE_header", "struct_p_e__header.html", "struct_p_e__header" ],
    [ "Section_header", "struct_section__header.html", "struct_section__header" ],
    [ "PE_file", "struct_p_e__file.html", "struct_p_e__file" ],
    [ "MAIN_OFFSET", "_p_efile_8h.html#a9cc3f03c591339e6f34f2e15e4271b87", null ],
    [ "find_section", "_p_efile_8h.html#afe777b11891d55772565cd0ce10e21af", null ],
    [ "go", "_p_efile_8h.html#a4be0c7e3de286e4128bd418f05d4c392", null ],
    [ "read_PE", "_p_efile_8h.html#a3902798296e469f4bb79783453627360", null ],
    [ "write_PE", "_p_efile_8h.html#a827c2a1c4edab5b18215b665b4e6ce5f", null ]
];